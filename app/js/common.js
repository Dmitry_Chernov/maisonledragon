$(function() {



    // Mobile MENU

    $('#menu_s').slicknav({
        appendTo: '.adaptive_menu',
        duration: '50',

        beforeOpen: function () {

            $('.logo').fadeOut(200);

        },

        afterClose: function () {

            if( $('.slicknav_menu a').hasClass('slicknav_collapsed') ){$('.logo').fadeIn(); }
        }
    });

    //  FOOTER ARROW

    $('#down').click(function() {



        if($(this).hasClass('fa-arrow-circle-o-down')){
            $('body').scrollTo('100%',1000,{axis:'y'});
            $(this).toggleClass('fa-arrow-circle-o-down fa-arrow-circle-o-up');
            if(window.matchMedia('(max-width: 768px)').matches){
                $('.flex-row-right, .flex-row-left').removeClass('hidden-xs').addClass('col-xs-12');
            }
        } else {
            $('body').scrollTo('0%',1000,{axis:'y'});
            $(this).toggleClass('fa-arrow-circle-o-up fa-arrow-circle-o-down');
            if(window.matchMedia('(max-width: 768px)').matches){
                $('.flex-row-right, .flex-row-left').removeClass('col-xs-12').addClass('hidden-xs');
            }
        }
    });


    // PRESS CAROUSEL

    $('.press_owl_carousel').owlCarousel({
        loop: true,
        items : 4,
        nav: true,
        navText: ['<i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>','<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>'],
        itemsDesktop : [1180],
        itemsDesktopSmall : [973,3],
        responsive: {
            0: {items: 1},
            468: {items:2},
            768: {items: 4}
        }
    });

    // IMAGE SLIDER
    if(window.location.pathname === '/lounge.html'){

        var bgImages =[
            'img/lounge/lounge_1.jpg',
            'img/lounge/lounge_2.jpg',
            'img/lounge/lounge_3.jpg',
            'img/lounge/lounge_4.jpg',
            'img/lounge/lounge_5.jpg'
        ];


    } else if (window.location.pathname === '/kitchen.html'){
        var bgImages = [
            'img/kitchen/kitchen_1.jpg',
            'img/kitchen/kitchen_2.jpg',
            'img/kitchen/kitchen_3.jpg'
        ];

    } else if (window.location.pathname === '/outside.html'){
        var bgImages = [
            'img/outside/outside_1.jpg',
            'img/outside/outside_2.jpg',
            'img/outside/outside_3.jpg'
        ];

    } else if (window.location.pathname === '/breakfast.html'){
        var bgImages = [
            'img/breakfast/breakfast_1.jpg',
            'img/breakfast/breakfast_2.jpg'
        ];

    } else if (window.location.pathname === '/rooms.html'){
        var bgImages = [
            'img/rooms/rooms_1.jpg',
            'img/rooms/rooms_2.jpg',
            'img/rooms/rooms_3.jpg',
            'img/rooms/rooms_4.jpg',
            'img/rooms/rooms_5.jpg',
            'img/rooms/rooms_6.jpg',
            'img/rooms/rooms_7.jpg',
            'img/rooms/rooms_8.jpg'
        ];

    } else if (window.location.pathname === '/bruges.html' || window.location.pathname === '/press.html' || window.location.pathname === '/restaurants.html') {

        var bgImages = [
            'img/bruges/bruges_1.jpg',
            'img/bruges/bruges_2.jpg',
            'img/bruges/bruges_3.jpg',
            'img/bruges/bruges_4.jpg',
            'img/bruges/bruges_5.jpg',
            'img/bruges/bruges_6.jpg',
            'img/bruges/bruges_7.jpg'

        ];

    }

    var btns = bgImages.length;

    $('#header').css('background-image', 'url(' + bgImages[0] + ')').css('transition','all 1s ease');


    for(var i=1; i <= btns; i++) {

        $('.bg_dots ul').append('<li class="bg_dot btn'+i+'"></li>')
    }


    $('.bg_dot').click(function () {


        if(window.matchMedia('(max-width: 768px)').matches){ $('.logo').addClass('disp_none');}


        for(var i=1; i <= btns; i++) {

            if( $(this).hasClass('btn'+i) ){
                $('#header').css('background-image', 'url(' + bgImages[i-1] + ')');
            }

        }




        $('.bg_dots ul').children().removeClass('active');
        $(this).addClass('active');
    });

});
// Active menu

var location_href = window.location.pathname.substr(1);

$('#menu_s li').each( function () {

    var link = $(this).find('a').attr('href');


    if(link == location_href && location_href != '' ){
        $(this).addClass('active');
    }

});

// RESTAURANTS SORT

var containerEl = $('.container_mix');

var mixer = mixitup(containerEl);